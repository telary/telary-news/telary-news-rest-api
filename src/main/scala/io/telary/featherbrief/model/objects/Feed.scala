package io.telary.featherbrief.model.objects

case class Feed(
                 name : String,
                 url : String,
                 publicationFrequency : Int,
                 lastPublicationDate : Long,
                 isInStandby : Boolean,
                 language : String,
                 description : String,
                 favicon : String,
                 createdBy : String,
                 parsable : Boolean
               )
object FeedField {
  val NAME : String = "name"
  val URL : String = "url"
  val PUBLICATION_FREQUENCY : String = "publicationFrequency"
  val LAST_PUBLICATION_DATE : String = "lastPublicationDate"
  val IS_IN_STANDBY : String = "isInStandby"
  val LANGUAGE : String = "language"
  val DESCRIPTION : String = "description"
  val FAVICON : String = "favicon"
  val CREATED_BY : String = "createdBy"
  val PARSABLE : String = "parsable"
}