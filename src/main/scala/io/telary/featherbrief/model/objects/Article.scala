package io.telary.featherbrief.model.objects

case class Article (
                   url : String,
                   title : String,
                   byline : String,
                   dir : String,
                   content : String,
                   textContent : String,
                   length : Int,
                   excerpt : String,
                   siteName : String
                   )
object ArticleField {
  val URL : String = "url"
  val TITLE : String = "title"
  val BYLINE : String = "byline"
  val DIR : String = "dir"
  val CONTENT : String = "content"
  val TEXT_CONTENT : String = "textContent"
  val LENGTH : String = "length"
  val EXCERPT : String = "excerpt"
  val SITE_NAME : String = "siteName"
}