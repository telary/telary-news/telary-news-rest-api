package io.telary.featherbrief.model.objects

case class Topic (
                 name: String,
                 url: String
                 )
object TopicField {
  val TOPIC_NAME = "feedName"
}