package io.telary.featherbrief.model.repositories

import java.util.stream.Collectors

import io.telary.featherbrief.model.objects.{Topic, TopicField, UserFeedItem, UserFeedItemField}
import io.telary.featherbrief.service.es.Converter
import org.elasticsearch.action.DocWriteResponse.Result
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.action.search.{ClearScrollRequest, SearchRequest, SearchScrollRequest}
import org.elasticsearch.client.{RequestOptions, RestHighLevelClient}
import org.elasticsearch.common.unit.TimeValue
import org.elasticsearch.common.xcontent.XContentType
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.aggregations.AggregationBuilders
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms
import org.elasticsearch.search.{Scroll, SearchHit}
import org.elasticsearch.search.builder.SearchSourceBuilder

import scala.collection.JavaConverters._
import scala.collection.JavaConverters.mapAsScalaMapConverter
import scala.collection.mutable

object TopicRepository {
  val INDEX_NAME: String = "topic"

  def index(esClient: RestHighLevelClient, userFeedItem: UserFeedItem) : Boolean = {
    val indexRequest = new IndexRequest(INDEX_NAME)
    indexRequest.source(Converter.toJsonObject(userFeedItem).encodePrettily(), XContentType.JSON)
    val response = esClient.index(indexRequest, RequestOptions.DEFAULT)
    response.getResult == Result.CREATED
  }

  def getItemsForATopic(esClient: RestHighLevelClient, topicName: String, email: String, from: Int, to: Int) : List[mutable.Map[String, AnyRef]] = {
    val request = new SearchRequest(INDEX_NAME)
    val scroll = new Scroll(TimeValue.timeValueMinutes(1L))
    val searchSourceBuilder : SearchSourceBuilder = new SearchSourceBuilder()
      .size(50)
      .query(QueryBuilders.boolQuery()
        .must(QueryBuilders.matchQuery(UserFeedItemField.EMAIL + ".keyword", email))
        .must(QueryBuilders.matchQuery(UserFeedItemField.FEED_NAME + ".keyword", topicName)))

    request
      .source(searchSourceBuilder)
      .scroll(scroll)

    var response = esClient.search(request, RequestOptions.DEFAULT)
    var scrollId : String  = response.getScrollId
    var hits : Array[SearchHit] = response.getHits.getHits
    var results : List[mutable.Map[String, AnyRef]] = List()

    while(hits != null && hits.length > 0)
    {
      val hitsAsMap = hits.map(hit => {
        hit.getSourceAsMap.asScala
      })
      results = results ++ hitsAsMap
      val scrollRequest = new SearchScrollRequest(scrollId)
      scrollRequest.scroll(scroll)
      response = esClient.scroll(scrollRequest, RequestOptions.DEFAULT)
      scrollId = response.getScrollId
      hits = response.getHits.getHits
    }

    val clearScrollRequest = new ClearScrollRequest
    clearScrollRequest.addScrollId(scrollId)
    val clearScrollResponse = esClient.clearScroll(clearScrollRequest, RequestOptions.DEFAULT)
    val succeeded = clearScrollResponse.isSucceeded
    results.slice(from, to + from)
  }

  def getAllTopics(esClient: RestHighLevelClient, email: String) : List[mutable.Map[String, AnyRef]] = {
    val request = new SearchRequest(INDEX_NAME)
    val aggName = "distinct_topics"
    val searchSourceBuilder : SearchSourceBuilder = new SearchSourceBuilder()
      .size(0)
      .query(QueryBuilders.boolQuery()
        .must(QueryBuilders.matchQuery(UserFeedItemField.EMAIL + ".keyword", email)))
      .aggregation(AggregationBuilders.terms(aggName).field(TopicField.TOPIC_NAME + ".keyword").size(1000))
    request
      .source(searchSourceBuilder)
    val response = esClient.search(request, RequestOptions.DEFAULT)
    val agg : ParsedStringTerms = response.getAggregations.get(aggName)
    var topicsList = List[mutable.Map[String, AnyRef]]()
    agg.getBuckets.forEach(bucket => {
      topicsList = topicsList ++ List(mutable.Map(
        "topicName" -> bucket.getKey.toString.asInstanceOf[AnyRef],
        "topicUrl" -> "".asInstanceOf[AnyRef]
      ))
    })
    topicsList
  }
}
