package io.telary.featherbrief.model

object UserInfoFields {
  /**
  {
      "nickname":"jean",
      "name":"jean@email.fr",
      "picture":"https://s.gravatar.com/avatar/592f.....png",
      "updated_at":"2020-04-18T08:37:44.942Z",
      "email":"jean@email.fr",
      "email_verified":false,
      "iss":"https://.............com/",
      "sub":"a|a",
      "aud":"aaaa",
      "iat":1580000000,
      "exp":1580000000,
      "nonce":"Base64String=="
     }
   */
  val NICKNAME : String = "nickname"
  val NAME : String = "name"
  val PICTURE : String = "picture"
  val UPDATED_AT : String = "updated_at"
  val EMAIL : String = "email"
  val EMAIL_VERIFIED : String = "email_verified"
  val ISS : String = "iss"
  val SUB : String = "sub"
  val AUD : String = "aud"
  val IAT : String = "iat"
  val EXP : String = "exp"
  val NONCE : String = "nonce"
}
