package io.telary.featherbrief.listener.actions

import java.util.Calendar

trait FeedActions extends FeedFrequencies {

  /**
   * Determines what is the frequency of publication of the feed that we need to check for update
   * @param now: the current date
   * @return the frequency
   */
  def determineFrequencyForRun(now : Calendar) : Int = {
    val currentMinute = now.get(Calendar.MINUTE)
    val currentHour = now.get(Calendar.HOUR_OF_DAY)
    val currentDayOfWeek = now.get(Calendar.DAY_OF_WEEK)
    if (currentDayOfWeek == Calendar.SUNDAY && currentHour == 1 && currentMinute == 0) {
      WEEKLY
    } else if (currentHour == 1 && currentMinute == 30 ) {
      DAILY
    } else if (currentMinute == 0) {
      HOURLY
    } else if (currentMinute % 30 == 10) {
      HALF_HOURLY
    } else if (currentMinute % 10 == 0) {
      TENTH_MINUTE
    } else {
      EVERY_MINUTE
    }
  }

  def assignFrequency(minPubDate: Long, maxPubDate: Long, size: Int) : Int = {
    if (size == 0) {
      INVALID
    }
    if (maxPubDate == minPubDate || size < 5) {
      // By default if the website is new or as only a few feed items we retrieve it DAILY
      DAILY
    } else {
      val publishFrequency = (((maxPubDate - minPubDate) / size) / 1000).toInt
      if (publishFrequency > 500000) {
        WEEKLY
      } else if (publishFrequency >= 86400) {
        DAILY
      } else if (publishFrequency >= 3600) {
        HOURLY
      } else if (publishFrequency >= 1800) {
        HALF_HOURLY
      } else if (publishFrequency >= 600) {
        TENTH_MINUTE
      } else {
        EVERY_MINUTE
      }
    }
  }

  /**
   * Check if a feed is in standby or not
   * @param frequency : publication frequency
   * @param maxPubDate : date of the last update of a feed
   * @return
   */
  def isInStandby(frequency: Int, maxPubDate: Long) : Option[Int] = {
    val now = Calendar.getInstance().toInstant.toEpochMilli
    val standbyFrequency = assignFrequency(maxPubDate, now, 1)
    if (standbyFrequency >= DAILY && standbyFrequency >= frequency) {
      Some(DAILY)
    } else {
      None
    }
  }

  def getFeedsForFrequency(frequency : Int) : List[String] = {
    List[String]()
  }
}
