package io.telary.featherbrief.server

import com.google.common.net.HttpHeaders
import io.telary.featherbrief.controller.utils.EsClientWrapper
import io.telary.featherbrief.controller.{DefaultRouteHandler, DeleteFeedHandler, FindFeedRouteHandler, GetBundlesHandler, GetExploreFeedsHandler, GetFeedItemHandler, GetFeedsHandler, GetTopicHandler, GetTopicsHandler, PostFeedHandler, PutFeedHandler, PutFeedItemHandler, PutOpmlHandler, PutTopicHandler, ReadabilityRouteHandler}
import io.telary.featherbrief.model.objects.Indices
import io.telary.featherbrief.model.repositories.AccessLogRepository
import io.telary.featherbrief.service.{AuthService, ConfigFile, NodeClient}
import io.vertx.core.json.JsonObject
import io.vertx.core.{Vertx, VertxOptions}
import io.vertx.ext.web.client.{WebClient, WebClientOptions}
import io.vertx.ext.web.handler.{BodyHandler, CorsHandler, StaticHandler}
import io.vertx.ext.web.{Router, RoutingContext}
import org.apache.http.HttpHost
import org.apache.http.auth.{AuthScope, UsernamePasswordCredentials}
import org.apache.http.impl.client.BasicCredentialsProvider
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder
import org.elasticsearch.client.{RestClient, RestHighLevelClient}

object Server extends DefaultRouteHandler
  with ReadabilityRouteHandler
  with FindFeedRouteHandler
{

  def run(configFilePath: String, publicKeyPath: String, isDebugEnabled: Boolean, debugEmail: String): Unit = {
    val vOptions = new VertxOptions
    if (isDebugEnabled) {
      vOptions.setBlockedThreadCheckInterval(1000 * 60 * 60)
    }
    val vertx = Vertx.vertx(vOptions)
    val server = vertx.createHttpServer
    val globalRouter = Router.router(vertx)
    globalRouter.route().handler(CorsHandler.create("*")
      .allowedMethod(io.vertx.core.http.HttpMethod.GET)
      .allowedMethod(io.vertx.core.http.HttpMethod.POST)
      .allowedMethod(io.vertx.core.http.HttpMethod.OPTIONS)
      .allowedMethod(io.vertx.core.http.HttpMethod.PUT)
      .allowedMethod(io.vertx.core.http.HttpMethod.DELETE)
      .allowedHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_METHODS)
      .allowedHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN)
      .allowedHeader(HttpHeaders.ACCESS_CONTROL_ALLOW_HEADERS)
      .allowedHeader(HttpHeaders.AUTHORIZATION)
      .allowedHeader(HttpHeaders.CONTENT_TYPE))
    globalRouter.route.handler(BodyHandler.create())

    val config = ConfigFile.parse(configFilePath)
    val publicKey = ConfigFile.read(publicKeyPath)

    val options = new WebClientOptions
    options.setUserAgent(config.userAgent)
    options.setKeepAlive(false)
    val httpClient = WebClient.create(vertx, options)

    val credentialsProvider = new BasicCredentialsProvider
    credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(config.esUsername, config.esPassword))

    val builder = RestClient.builder(new HttpHost(config.esHost, config.esPort.toInt))
      .setHttpClientConfigCallback((httpClientBuilder: HttpAsyncClientBuilder) => {
        httpClientBuilder.setDefaultCredentialsProvider(credentialsProvider)
      })

    val esClient : RestHighLevelClient = new RestHighLevelClient(builder)
    val nodeClient : NodeClient = NodeClient(config.nodeServerHost, config.nodeServerPort.toInt)

    val securedRouter : Router = Router.router(vertx)
    securedRouter.route("/*").handler((routingContext: RoutingContext) => {
      val authorizationHeader = routingContext.request().getHeader("authorization")
      if (authorizationHeader != null && !authorizationHeader.isEmpty) {
        val authToken = try {
          authorizationHeader.split(' ')(1)
        } catch {
          case oob : ArrayIndexOutOfBoundsException => ""
        }
        if(AuthService.isValid(authToken, publicKey)) {
          val userInfo = AuthService.decode(authToken, publicKey)
          val hashedEmail = AuthService.hashEmail(config.salt, userInfo.email)
          routingContext.request().params().add("email", hashedEmail)
          routingContext.next()
          val accessLogRepository = new AccessLogRepository(Indices.ACCESS_LOG)
          accessLogRepository.save(esClient, hashedEmail)
        } else {
          val json : JsonObject = new JsonObject
          json.put("error", "couldn't authorize user")
          routingContext.response.end(json.encodePrettily())
        }
      } else {
        if (isDebugEnabled) {
          routingContext.request().params().add("email", AuthService.hashEmail(config.salt, debugEmail))
          routingContext.next()
        } else {
          val json: JsonObject = new JsonObject
          json.put("error", "couldn't authorize user")
          routingContext.response.end(json.encodePrettily())
        }
      }
    })

    val indicesMap = Map(
      Indices.USER_FEED -> Indices.USER_FEED,
      Indices.USER_FEED_ITEM -> Indices.USER_FEED_ITEM,
      Indices.FEED -> Indices.FEED,
      Indices.ARTICLE -> Indices.ARTICLE,
      Indices.ACCESS_LOG -> Indices.ACCESS_LOG,
      Indices.BUNDLE -> Indices.BUNDLE
    )
    val esClientWrapper = EsClientWrapper(esClient, indicesMap)

    /**
     * Secured routes behind /secured
     */
    securedRouter.post("/feeds").handler(PostFeedHandler.handle(_, httpClient, esClientWrapper))
    securedRouter.get("/feeds").handler(GetFeedsHandler.handle(_, esClient = esClientWrapper))
    securedRouter.delete("/feeds").handler(DeleteFeedHandler.handle(_, esClient = esClientWrapper))
    securedRouter.put("/feeds").handler(PutFeedHandler.handle(_, esClient = esClientWrapper))
    securedRouter.get("/feeditems").handler(GetFeedItemHandler.handle(_, esClient = esClientWrapper))
    securedRouter.put("/feeditems").handler(PutFeedItemHandler.handle(_, esClient = esClientWrapper))
    securedRouter.put("/opml").handler(PutOpmlHandler.handle(_, esClient = esClientWrapper))
    securedRouter.get("/topics").handler(GetTopicsHandler.handle(_, esClient = esClientWrapper))
    securedRouter.get("/topic").handler(GetTopicHandler.handle(_, esClient = esClientWrapper))
    securedRouter.put("/topic").handler(PutTopicHandler.handle(_, esClient = esClientWrapper))
    securedRouter.get("/explore/feeds").handler(GetExploreFeedsHandler.handle(_, esClient = esClientWrapper))
    securedRouter.get("/bundles").handler(GetBundlesHandler.handle(_, esClient = esClientWrapper))
    /** Async routes */
    securedRouter.get("/find/feed").handler(findFeedUrl(_, httpClient, nodeClient = nodeClient))
    /** Placeholder routes */
    securedRouter.route("/profile").handler(defaultRouteHandler(_, esClient))

    /**
     * Unsecured routes
     */
    /** Async routes */
    globalRouter.get("/parse").handler(parseArticle(_, httpClient, nodeClient, esClient))
    /** Placeholder routes */
    globalRouter.get("/").handler(defaultRouteHandler(_, esClient))
    globalRouter.get("/error").handler(defaultRouteHandler(_, esClient))
    globalRouter.get("/static/*").handler(StaticHandler.create("favicons"))

    globalRouter.mountSubRouter("/secured", securedRouter)

    server.requestHandler(globalRouter).listen(config.serverPort.toInt)
  }
}
