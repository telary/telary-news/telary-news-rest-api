package io.telary.featherbrief.service

case class NodeClient(
                     host : String,
                     port : Int
                     )
