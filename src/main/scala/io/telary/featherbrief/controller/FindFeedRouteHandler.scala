package io.telary.featherbrief.controller

import io.telary.featherbrief.service.NodeClient
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.client.WebClient

trait FindFeedRouteHandler {

  def findFeedUrl(routingContext: RoutingContext, client: WebClient, nodeClient: NodeClient): Unit = {
    val response = routingContext.response()
    val request = routingContext.request()
    val url = request.getParam("url")
    if (url.nonEmpty) {
      client.get(nodeClient.port, nodeClient.host, "/feed?url=%s".format(url))
        .send(ar => {
          if (ar.succeeded) { // Obtain response
            val json = new JsonObject(ar.result.body)
            response.end(json.encodePrettily())
          } else {
            response.end("{}")
          }
        })
    } else {
      response.end("{}")
    }
  }
}
