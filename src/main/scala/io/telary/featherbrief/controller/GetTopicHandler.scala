package io.telary.featherbrief.controller

import io.telary.featherbrief.controller.utils.{EsClientWrapper, RouteHandler, RouteParams, RouteResponse}
import io.telary.featherbrief.model.repositories.TopicRepository
import io.telary.featherbrief.service.NodeClient
import io.vertx.ext.web.FileUpload
import io.vertx.ext.web.client.WebClient
import org.elasticsearch.client.RestHighLevelClient

object GetTopicHandler extends RouteHandler {
  /**
   * handle the route processing
   *
   * @param routeParams : path and body params
   * @return the response
   */
  override def process(routeParams: RouteParams,
                                 webClient: Option[WebClient],
                                 esWrapper: Option[EsClientWrapper],
                                 nodeClient: Option[NodeClient],
                                 uploadedFiles: Option[List[FileUpload]]): RouteResponse = {
    val email = getEmail(routeParams)
    val name = routeParams.param.getOrElse("name", "").toString
    if (name.nonEmpty) {
      val from = routeParams.param.getOrElse("from", "0").toString.toInt
      val to = routeParams.param.getOrElse("to", "250").toString.toInt
      val feedItems = TopicRepository.getItemsForATopic(esWrapper.get.esClient, name, email, from, to)
      RouteResponse(200, list = feedItems, listName = "items")
    } else {
      RouteResponse(406, content = Map("error" -> "Invalid request"))
    }
  }
}
