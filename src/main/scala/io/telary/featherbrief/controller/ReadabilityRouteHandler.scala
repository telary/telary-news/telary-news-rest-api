package io.telary.featherbrief.controller

import io.telary.featherbrief.model.objects.{ArticleField, Indices}
import io.telary.featherbrief.model.repositories.ArticleRepository
import io.telary.featherbrief.service.NodeClient
import io.telary.featherbrief.service.es.Converter
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.client.WebClient
import org.elasticsearch.client.RestHighLevelClient

trait ReadabilityRouteHandler {
  def parseArticle(routingContext: RoutingContext, client: WebClient, nodeClient: NodeClient, esClient: RestHighLevelClient) : Unit = {
    val response = routingContext.response()
    val request = routingContext.request()
    val url = request.getParam("url")
    val articleRepository = new ArticleRepository(Indices.ARTICLE)
    val savedArticle = articleRepository.get(esClient, url)
    if (savedArticle.isDefined) {
      response.end(Converter.toJsonObject(savedArticle.get).encodePrettily())
    } else {
      client.get(nodeClient.port, nodeClient.host , "/readability?url=%s".format(url))
        .send(ar => {
          if (ar.succeeded) { // Obtain response
            val json = new JsonObject(ar.result.body)
            json.put(ArticleField.URL, url)
            articleRepository.save(esClient, json)
            response.end(json.encodePrettily())
          } else {
            response.end("{}")
          }
        })
    }
  }
}
