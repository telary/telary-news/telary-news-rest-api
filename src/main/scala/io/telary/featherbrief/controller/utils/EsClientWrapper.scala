package io.telary.featherbrief.controller.utils

import org.elasticsearch.client.RestHighLevelClient

case class EsClientWrapper(
                          esClient : RestHighLevelClient,
                          indices : Map[String, String]
                          )
