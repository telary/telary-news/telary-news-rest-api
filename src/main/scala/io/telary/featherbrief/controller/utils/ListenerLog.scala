package io.telary.featherbrief.controller.utils

import java.time.LocalDateTime

import io.vertx.core.json.JsonObject

class ListenerLog (
                    val action : String,
                    val errorMessage : String = ""
                  ) {
  def toJson: JsonObject = {
    new JsonObject()
      .put("date", LocalDateTime.now().toLocalTime.toString)
      .put("action", action)
      .put("error", errorMessage)
  }

  def log() : Unit = {
    println(toJson.toString)
  }
}