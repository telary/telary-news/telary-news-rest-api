package io.telary.featherbrief.controller.utils

case class RouteParams(path : Map[String, String], body : Map[String, Any], param : Map[String, Any])
