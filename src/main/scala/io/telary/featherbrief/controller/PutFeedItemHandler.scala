package io.telary.featherbrief.controller

import java.net.URLDecoder
import java.nio.charset.Charset
import java.util

import io.telary.featherbrief.controller.utils.{EsClientWrapper, RouteHandler, RouteParams, RouteResponse}
import io.telary.featherbrief.model.objects.{Indices, UserFeedItemField}
import io.telary.featherbrief.model.repositories.UserFeedItemRepository
import io.telary.featherbrief.service.NodeClient
import io.vertx.core.json.JsonArray
import io.vertx.ext.web.FileUpload
import io.vertx.ext.web.client.WebClient
import io.vertx.lang.scala.json.JsonArray
import org.elasticsearch.client.RestHighLevelClient

import scala.collection.JavaConverters._

object PutFeedItemHandler extends RouteHandler {
  /**
   * handle the route processing
   *
   * @param routeParams : path and body params
   * @return the response
   */
  override def process(routeParams: RouteParams,
                       webClient: Option[WebClient],
                       esWrapper: Option[EsClientWrapper],
                       nodeClient: Option[NodeClient],
                       uploadedFiles: Option[List[FileUpload]]
                                ): RouteResponse = {
    val articlesUrl : List[String] = if (routeParams.body.contains("articles")) {
      routeParams.body("articles").asInstanceOf[util.ArrayList[String]].asScala.toList
    } else {
      List()
    }
    val email = getEmail(routeParams)
    val userFeedItemRepository = new UserFeedItemRepository(esWrapper.get.indices(Indices.USER_FEED_ITEM))
    if (routeParams.param.contains("markAs")) {
      val action = routeParams.param("markAs")
      if (action == "shown") {
        userFeedItemRepository.markAsShown(esWrapper.get.esClient, email, articlesUrl)
      } else if (action == "read") {
        userFeedItemRepository.markAsRead(esWrapper.get.esClient, email, articlesUrl)
      } else if (action == "saved") {
        userFeedItemRepository.markAsSaved(esWrapper.get.esClient, email, articlesUrl)
      } else if (action == "upvote") {
        userFeedItemRepository.markAsUpvoted(esWrapper.get.esClient, email, articlesUrl)
      } else if (action == "downvote") {
        userFeedItemRepository.markAsDownvoted(esWrapper.get.esClient, email, articlesUrl)
      }
    } else if (routeParams.param.contains("unmarkAs")) {
      val action = routeParams.param("unmarkAs")
      if (action == "shown") {
        userFeedItemRepository.unmarkAsShown(esWrapper.get.esClient, email, articlesUrl)
      } else if (action == "read") {
        userFeedItemRepository.unmarkAsRead(esWrapper.get.esClient, email, articlesUrl)
      } else if (action == "saved") {
        userFeedItemRepository.unmarkAsSaved(esWrapper.get.esClient, email, articlesUrl)
      } else if (action == "upvote") {
        userFeedItemRepository.unmarkAsUpvoted(esWrapper.get.esClient, email, articlesUrl)
      } else if (action == "downvote") {
        userFeedItemRepository.unmarkAsDownvoted(esWrapper.get.esClient, email, articlesUrl)
      }
    }
    RouteResponse(200)
  }
}
