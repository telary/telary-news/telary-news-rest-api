package io.telary.featherbrief.model.repositories

import io.telary.featherbrief.model.objects.{ArticleField, Indices}
import io.vertx.lang.scala.json.JsonObject
import org.scalatest._
import org.scalatest.matchers.should.Matchers

class ArticleRepositoryTest extends FlatSpec
  with Matchers with RepositorySuite {

  "Using article actions" should "update ES database" in {
    val esClient = RepositoryUtil.esClient()
    val articleRepository = new ArticleRepository(Indices.ARTICLE_TEST)
    val article = new JsonObject
    article.put(ArticleField.URL, "article-1")
    article.put(ArticleField.CONTENT, "test content")
    articleRepository.save(esClient, article)
    Thread.sleep(1000)
    val existingArticle = articleRepository.get(esClient, "article-1")
    val emptyArticle = articleRepository.get(esClient, "article-2")
    existingArticle.isDefined shouldBe true
    existingArticle.get.url shouldBe "article-1"
    emptyArticle.isDefined shouldBe false
  }
}
