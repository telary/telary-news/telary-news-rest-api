package io.telary.featherbrief.model.repositories

import io.telary.featherbrief.model.objects.{Indices}
import org.elasticsearch.action.search.{SearchRequest, SearchResponse}
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.builder.SearchSourceBuilder
import org.scalatest._
import org.scalatest.matchers.should.Matchers

class AccessLogRepositoryTest extends FlatSpec
  with Matchers with RepositorySuite {

  "Using AccessLog save action" should "update ES database" in {
    val esClient = RepositoryUtil.esClient()
    val email = "test_email"
    val accessLogRepository = new AccessLogRepository(Indices.ACCESS_LOG_TEST)
    accessLogRepository.save(esClient, email)
    Thread.sleep(1000)
    val request = new SearchRequest(Indices.ACCESS_LOG_TEST)
    val searchSourceBuilder : SearchSourceBuilder = new SearchSourceBuilder()
    searchSourceBuilder.query(QueryBuilders.termQuery("email.keyword", email))
    searchSourceBuilder.size(1)
    request.source(searchSourceBuilder)
    val searchResponse : SearchResponse = esClient.search(request, RequestOptions.DEFAULT);
    val hits = searchResponse.getHits.getHits
    hits.nonEmpty shouldBe true
    hits(0).getSourceAsMap.get("email") shouldEqual email
  }
}
