package io.telary.featherbrief.controller

import io.telary.featherbrief.controller.utils.{EsClientWrapper, RouteParams}
import io.telary.featherbrief.model.objects.{FeedField, Indices}
import io.telary.featherbrief.model.repositories.{FeedRepository, RepositorySuite, RepositoryUtil}
import org.scalatest._
import org.scalatest.matchers.should.Matchers

class GetExploreFeedsHandlerTest extends FlatSpec
  with Matchers with RepositorySuite {

  "[GET] explore feeds" should "return some feeds to add" in {
    val esClient = RepositoryUtil.esClient()
    val esWrapper = Some(EsClientWrapper(esClient,
      Map[String, String](
        Indices.FEED -> Indices.FEED_TEST
      )))

    val feedRepository = new FeedRepository(Indices.FEED_TEST)
    val feedUrl = "http://testurl.com/"
    feedRepository.create(esClient, feedUrl + "1", "Test Feed 1", "", "",
      2, None, "", "", parsable = true)
    feedRepository.create(esClient, feedUrl + "2", "Test Feed 2", "", "",
      1, None, "", "", parsable = true)
    Thread.sleep(1000)
    val response2 = GetExploreFeedsHandler.process(null, null, esWrapper, null, null)
    response2.listName shouldBe "feeds"
    response2.list.size shouldBe 2
    response2.list.head(FeedField.URL) shouldBe feedUrl + "1"
    response2.list(1)(FeedField.URL) shouldBe feedUrl + "2"
  }
}
