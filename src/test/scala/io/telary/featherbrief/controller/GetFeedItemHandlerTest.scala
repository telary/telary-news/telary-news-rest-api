package io.telary.featherbrief.controller

import io.telary.featherbrief.controller.utils.{EsClientWrapper, RouteParams}
import io.telary.featherbrief.model.objects.{Indices, UserFeedItem}
import io.telary.featherbrief.model.repositories.{RepositorySuite, RepositoryUtil, UserFeedItemRepository}
import org.scalatest._
import org.scalatest.matchers.should.Matchers

class GetFeedItemHandlerTest extends FlatSpec
  with Matchers with RepositorySuite {

  "[GET] feed item" should "return the feed items asked" in {
    val esClient = RepositoryUtil.esClient()
    val esWrapper = Some(EsClientWrapper(esClient,
      Map[String, String](
        Indices.USER_FEED_ITEM -> Indices.USER_FEED_ITEM_TEST
      )))
    val userFeedItemRepository = new UserFeedItemRepository(Indices.USER_FEED_ITEM_TEST)
    val email = "test@email.com"
    val otherEmail = "otherUser@email.com"
    val ufiList = List(
      new UserFeedItem(email, "feed1", "feed-url", "1", "", "", 1L,
        "", "", 1, false, false, false, false,
        false, "", true),
      new UserFeedItem(email, "feed1", "feed-url", "2", "", "", 1L,
        "", "", 2, false, false, false, false,
        false, "", true),
      new UserFeedItem(email, "feed2", "feed-url-2", "3", "", "", 1L,
        "", "", 3, false, false, false, false,
        false, "", true),
      new UserFeedItem(email, "feed2", "feed-url-2", "4", "", "", 1L,
        "", "", 4, false, false, false, false,
        false, "", true),
      new UserFeedItem(email, "feed3", "feed-url", "5", "", "", 1L,
        "", "", 4, false, false, false, false,
        false, "", true),
      new UserFeedItem(email, "feed3", "feed-url", "6", "", "", 1L,
        "", "", 4, false, false, false, false,
        false, "", true),
      new UserFeedItem(email, "feed4", "feed-url", "7", "", "", 1L,
        "", "", 4, false, false, true, false,
        false, "", true),
      new UserFeedItem(email, "feed4", "feed-url", "8", "", "", 1L,
        "", "", 4, false, false, true, false,
        false, "", true),
      new UserFeedItem(otherEmail, "feed4", "feed-url", "7", "", "", 1L,
        "", "", 4, false, false, true, false,
        false, "", true)
    )
    userFeedItemRepository.insertAll(esClient, ufiList)
    Thread.sleep(1000)
    testSimpleCase(esWrapper)
    testSizeArg(esWrapper)
    testSavedFilter(esWrapper)
    testSavedFilterAndSize(esWrapper)
  }

  def testSimpleCase(esWrapper : Option[EsClientWrapper]): Unit = {
    val routeParams = RouteParams(
      path = null,
      body = null,
      param = Map[String, String](
        "email" -> "test@email.com",
      )
    )
    val response = GetFeedItemHandler.process(routeParams, null, esWrapper, null, null)
    response.list.size shouldBe 5
  }

  def testSizeArg(esWrapper : Option[EsClientWrapper]) : Unit = {
    val routeParams = RouteParams(
      path = null,
      body = null,
      param = Map[String, String](
        "email" -> "test@email.com",
        "size" -> "10"
      )
    )
    val response = GetFeedItemHandler.process(routeParams, null, esWrapper, null, null)
    response.list.size shouldBe 6
  }

  def testSavedFilter(esWrapper : Option[EsClientWrapper]) : Unit = {
    val routeParams = RouteParams(
      path = null,
      body = null,
      param = Map[String, String](
        "email" -> "test@email.com",
        "filter" -> "saved"
      )
    )
    val response = GetFeedItemHandler.process(routeParams, null, esWrapper, null, null)
    response.list.size shouldBe 2
  }

  def testSavedFilterAndSize(esWrapper : Option[EsClientWrapper]) : Unit = {
    val routeParams = RouteParams(
      path = null,
      body = null,
      param = Map[String, String](
        "email" -> "test@email.com",
        "feed" -> "feed-url",
        "size" -> "1",
        "filter" -> "saved"
      )
    )
    val response = GetFeedItemHandler.process(routeParams, null, esWrapper, null, null)
    response.list.size shouldBe 1
  }
}
