import java.net.URI
import java.net.http.HttpResponse

import io.telary.featherbrief.service.{AuthService, ConfigFile}
import io.vertx.core.{MultiMap, Vertx}
import io.vertx.ext.web.client.{WebClient, WebClientOptions}
import io.vertx.lang.scala.json.JsonObject
import org.scalatest._
import org.scalatest.matchers.should.Matchers

class JwtTest extends FlatSpec with Matchers {
  /*"JwtRouteHandler" should "validate my JWT token" in {
    val vertx = Vertx.vertx
    val options = new WebClientOptions
    val config = ConfigFile.parse("src/main/resources/CONFIG")
    options.setUserAgent(config.userAgent)
    options.setKeepAlive(false)
    val httpClient = WebClient.create(vertx, options)

    val jsonObject = new JsonObject()
      .put("client_id", config.auth0ClientId)
      .put("client_secret", config.auth0ClientSecret)
      .put("audience", config.auth0Audience)
      .put("grant_type", config.auth0GrantType)

    var isCallDone = false

    val auth0Uri = new URI(config.auth0Url)
    httpClient.post(443, auth0Uri.getHost, auth0Uri.getPath)
      .putHeader("content-type", "application/json")
      .ssl(true)
      .sendJsonObject(jsonObject, ar => {
        try {
          if (ar.succeeded) { // Obtain response
            println(ar.result.bodyAsString())
            val body = ar.result.bodyAsJsonObject()
            val jwtToken = body.getString("access_token")
            AuthService.isValid(jwtToken, config.jwtSecret) shouldBe true
          } else {
            println("An error occured during obtaining the jwt token")
          }
        } finally {
          isCallDone = true
        }
      })
    while(!isCallDone) {
      Thread.sleep(100)
      println("Waiting for result...")
    }
  }*/

  /*"JwtRouteHandler" should "invalidate my JWT token" in {
    val config = ConfigFile.parse("src/main/resources/CONFIG")
    AuthService.isValid("FAKE TOKEN", config.jwtSecret) shouldBe false
  }*/

  /*"JwtRouteHandler" should "validate my static JWT token" in {
    val pubKey = ConfigFile.read("src/main/resources/PUB_KEY")
    AuthService.isValid(
      "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6ImEwVXFnclhVQTg2QnhtamZfZk5zUyJ9.eyJuaWNrbmFtZSI6ImNvbnN0YW50IiwibmFtZSI6ImNvbnN0YW50QGNuLWRzLmZyIiwicGljdHVyZSI6Imh0dHBzOi8vcy5ncmF2YXRhci5jb20vYXZhdGFyLzU5MmYzMmFiMDUxMjdlYWQ4YWQ4ZjFhNzg3MDIxZmYyP3M9NDgwJnI9cGcmZD1odHRwcyUzQSUyRiUyRmNkbi5hdXRoMC5jb20lMkZhdmF0YXJzJTJGY28ucG5nIiwidXBkYXRlZF9hdCI6IjIwMjAtMDUtMTdUMDk6Mjg6MTQuODczWiIsImVtYWlsIjoiY29uc3RhbnRAY24tZHMuZnIiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsImlzcyI6Imh0dHBzOi8vdGVsYXJ5LmV1LmF1dGgwLmNvbS8iLCJzdWIiOiJhdXRoMHw1ZTk5OGQ5ZGQ5ZTliMDBiZjJhZGM0YTQiLCJhdWQiOiJENkNMUzFQbTZ2UThSYVVrZ1c1MlNOdVdTNlFnWUpxWiIsImlhdCI6MTU4OTcwNzY5NSwiZXhwIjoxNTg5NzQzNjk1LCJub25jZSI6ImI0emxaTnFwSUk5Y3ZmOXlSa09MTDNVOUJvYy5TVlhBU3lLVXpEWmJ1UkUifQ.kQdPTQglgtS5D3qDL4nHbFWGMg9ZEMpxwHZYlXrsadvxSV0sAJuOb2trQ-p_BpJL4K45fotSwfmLGPpk1i1Rch2JvsPZTjueUsZIToSrwGviHlN5FXLkYEn0TObBlljVyDvbGPpkrEfAoLK3Ud2z4oNUtd7yQXkJNidJxxlIFqpRir2HfYgYFDyA0bWjFyLpns9DbgurNNC_WilD-UdTdAPaEkaiUd7TEVy6y8nHJ6cJrL8HjIkXP5X7-KkroeuqqDSJnTXey8Gc1ElR3NsNSo-74sgAp3jBc-_l1NaC6hMmKm9oT3k32ayni9616o1dfUaog2yF9Mdn-uakfaU-tA",
      pubKey)
  }*/

}
